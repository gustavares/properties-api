package routes

import (
	"github.com/portaria-app/properties-api/server/http"
	"github.com/valyala/fasthttp"
)

// TODO: move this handlers to a controller layer
func HealthcheckHandler() fasthttp.RequestHandler {
	return fasthttp.RequestHandler(func(ctx *fasthttp.RequestCtx) {
		ctx.Response.Header.Add("Content-Type", "application/json; charset=UTF-8")
		ctx.SetStatusCode(fasthttp.StatusOK)

		ctx.SetBodyString(`{"message": "OK"}`)
	})
}

func ReadinessHandler() fasthttp.RequestHandler {
	return fasthttp.RequestHandler(func(ctx *fasthttp.RequestCtx) {
		ctx.Response.Header.Add("Content-Type", "application/json; charset=UTF-8")
		ctx.SetStatusCode(fasthttp.StatusOK)

		ctx.SetBodyString(`{"message": "OK"}`)
	})
}

func HealthcheckRoutes() []http.Route {
	return []http.Route{
		{
			Path:    "/healthcheck",
			Method:  fasthttp.MethodGet,
			Handler: HealthcheckHandler(),
		},
		{
			Path:    "/readiness",
			Method:  fasthttp.MethodGet,
			Handler: ReadinessHandler(),
		},
	}
}
