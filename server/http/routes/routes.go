package routes

import "github.com/portaria-app/properties-api/server/http"

func Setup() *[]http.Route {
	allRoutes := [][]http.Route{
		HealthcheckRoutes(),
	}

	appendRoutes := []http.Route{}

	for _, routeGroup := range allRoutes {
		appendRoutes = append(appendRoutes, routeGroup...)
	}

	return &appendRoutes
}
