package domain

import "github.com/portaria-app/properties-api/internal/domain/entity"

//go:generate mockgen -source=owner.go -destination=../../infrastructure/repositories/mocks/owner_mock.go -package=mocks OwnerRepository
type OwnerRepository interface {
	Save(entity.Owner) error
}
