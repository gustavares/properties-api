package domain

import "github.com/portaria-app/properties-api/internal/domain/entity"

//go:generate mockgen -source=porteiro.go -destination=../../infrastructure/repositories/mocks/porteiro_mock.go -package=mocks PorteiroRepository
type PorteiroRepository interface {
	Save(entity.Porteiro) error
}
