package commands

import (
	"github.com/portaria-app/properties-api/internal/domain/entity"
	domain "github.com/portaria-app/properties-api/internal/domain/repositories"
	"github.com/portaria-app/properties-api/internal/domain/valueobject"
)

type RegisterPorteiroCommand struct {
	repository domain.PorteiroRepository
}

func (r *RegisterPorteiroCommand) Execute(name valueobject.Name, cpf valueobject.CPF, phone valueobject.Phone) error {
	porteiro := entity.NewPorteiro(name, phone, cpf)
	if err := r.repository.Save(*porteiro); err != nil {
		return err
	}

	return nil
}
