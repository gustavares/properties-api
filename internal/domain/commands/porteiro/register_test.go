package commands

import (
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/portaria-app/properties-api/internal/domain/valueobject"
	"github.com/portaria-app/properties-api/internal/infrastructure/repositories/mocks"
)

func TestRegisterPorteiroCommand_execute(t *testing.T) {
	name := valueobject.BuildName("Peter", "Parker")
	cpf := valueobject.BuildCPF("111.111.111-11")
	phone := valueobject.BuildPhone("+55", "21", "999999999")

	// porteiro := entity.NewPorteiro(name, phone, cpf)
	type fields struct {
		repository func(ctrl *gomock.Controller) *mocks.MockPorteiroRepository
	}
	type args struct {
		name  valueobject.Name
		cpf   valueobject.CPF
		phone valueobject.Phone
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "it should receive a name, cpf, and phone and return no errors",
			fields: fields{
				repository: func(ctrl *gomock.Controller) *mocks.MockPorteiroRepository {
					repo := mocks.NewMockPorteiroRepository(ctrl)
					repo.EXPECT().Save(gomock.Any()).Times(1).Return(nil)

					return repo
				},
			},
			args: args{
				name:  name,
				cpf:   cpf,
				phone: phone,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			r := &RegisterPorteiroCommand{
				repository: tt.fields.repository(ctrl),
			}
			if err := r.Execute(tt.args.name, tt.args.cpf, tt.args.phone); (err != nil) != tt.wantErr {
				t.Errorf("RegisterPorteiroCommand.execute() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
