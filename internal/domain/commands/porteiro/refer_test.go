package commands

import (
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/portaria-app/properties-api/internal/domain/valueobject"
	"github.com/portaria-app/properties-api/internal/infrastructure/repositories/mocks"
)

func TestReferOwnerCommand_Execute(t *testing.T) {
	name := valueobject.BuildName("Mary", "Jane")
	phone := valueobject.BuildPhone("+55", "21", "999999999")

	type fields struct {
		repository func(ctrl *gomock.Controller) *mocks.MockOwnerRepository
	}
	type args struct {
		name  valueobject.Name
		phone valueobject.Phone
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "it should receive a name and phone and return no errors",
			fields: fields{
				repository: func(ctrl *gomock.Controller) *mocks.MockOwnerRepository {
					repo := mocks.NewMockOwnerRepository(ctrl)
					repo.EXPECT().Save(gomock.Any()).Times(1).Return(nil)

					return repo
				},
			},
			args: args{
				name:  name,
				phone: phone,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			r := &ReferOwnerCommand{
				repository: tt.fields.repository(ctrl),
			}
			if err := r.Execute(tt.args.name, tt.args.phone); (err != nil) != tt.wantErr {
				t.Errorf("ReferOwnerCommand.Execute() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
