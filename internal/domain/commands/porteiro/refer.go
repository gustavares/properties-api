package commands

import (
	"github.com/portaria-app/properties-api/internal/domain/entity"
	domain "github.com/portaria-app/properties-api/internal/domain/repositories"
	"github.com/portaria-app/properties-api/internal/domain/valueobject"
)

type ReferOwnerCommand struct {
	repository domain.OwnerRepository
}

func (r *ReferOwnerCommand) Execute(name valueobject.Name, phone valueobject.Phone) error {
	owner := entity.NewOwnerReferral(name, phone)
	if err := r.repository.Save(*owner); err != nil {
		return err
	}

	return nil
}
