package valueobject

type Phone struct {
	countryCode string
	areaCode    string
	number      string
}

func BuildPhone(countryCode, areaCode, number string) Phone {
	return Phone{
		countryCode,
		areaCode,
		number,
	}
}

func (p *Phone) CountryCode() string {
	return p.countryCode
}

func (p *Phone) AreaCode() string {
	return p.areaCode
}

func (p *Phone) Number() string {
	return p.number
}
