package valueobject

import "fmt"

type Name struct {
	firstName string
	surname   string
}

func BuildName(firstName, surname string) Name {
	return Name{
		firstName,
		surname,
	}
}

func (n Name) FullName() string {
	return fmt.Sprintf(n.firstName + " " + n.surname)
}
