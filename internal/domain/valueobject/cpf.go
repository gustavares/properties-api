package valueobject

type CPF struct {
	value string
}

func BuildCPF(value string) CPF {
	return CPF{
		value,
	}
}

func (c *CPF) Value() string {
	return c.value
}

func (c *CPF) SetValue(value string) {
	c.value = value
}

func (c *CPF) isValid() bool {
	return false
}
