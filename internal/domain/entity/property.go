package entity

import "github.com/portaria-app/properties-api/internal/domain/valueobject"

type Property struct {
	address valueobject.Address
	owner   Owner
}

func NewProperty(address valueobject.Address, owner Owner) *Property {
	return &Property{
		address,
		owner,
	}
}
