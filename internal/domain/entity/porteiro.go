package entity

import (
	"errors"

	"github.com/portaria-app/properties-api/internal/domain/valueobject"
)

type Porteiro struct {
	name  valueobject.Name
	phone valueobject.Phone
	cpf   valueobject.CPF
}

func NewPorteiro(name valueobject.Name, phone valueobject.Phone, cpf valueobject.CPF) *Porteiro {
	return &Porteiro{
		name:  name,
		phone: phone,
		cpf:   cpf,
	}
}

func (p *Porteiro) RegisterPropertyOwner(owner Owner) error {
	return errors.New("not yet implemented")
}
