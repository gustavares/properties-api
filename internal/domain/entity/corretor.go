package entity

import "errors"

type Corretor struct {
}

func NewCorretor() *Corretor {
	return &Corretor{}
}

func (c *Corretor) UpdatePropertyInformation(property Property) error {
	return errors.New("not yet implemented")
}

func (c *Corretor) SchedulePropertyVisitWithOwner(property Property) error {
	return errors.New("not yet implemented")
}

func (c *Corretor) SchedulePropertyVisitWithBuyer(property Property, buyer Buyer) error {
	return errors.New("not yet implemented")
}
