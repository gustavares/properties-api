package entity

import (
	"errors"

	"github.com/portaria-app/properties-api/internal/domain/valueobject"
)

type Owner struct {
	name       valueobject.Name
	phone      valueobject.Phone
	cpf        valueobject.CPF
	properties []Property
}

func NewOwner(name valueobject.Name, phone valueobject.Phone, cpf valueobject.CPF, properties []Property) *Owner {
	return &Owner{
		name:       name,
		phone:      phone,
		cpf:        cpf,
		properties: properties,
	}
}

// Creates an Owner object for referral
func NewOwnerReferral(name valueobject.Name, phone valueobject.Phone) *Owner {
	return &Owner{
		name:  name,
		phone: phone,
	}
}

func (o *Owner) AcceptPropertyRegistration() error {
	return errors.New("not yet implemented")
}

func (o *Owner) RejectPropertyRegistration() error {
	return errors.New("not yet implemented")
}

func (o *Owner) AcceptOffer(offer Offer) error {
	return errors.New("not yet implemented")
}

func (o *Owner) RejectOffer(offer Offer) error {
	return errors.New("not yet implemented")
}

func (o *Owner) SendCounterOffer(counterOffer Offer, offer Offer) error {
	return errors.New("not yet implemented")
}
