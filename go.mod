module github.com/portaria-app/properties-api

go 1.16

require (
	github.com/fasthttp/router v1.4.0
	github.com/golang/mock v1.6.0 // indirect
	github.com/valyala/fasthttp v1.27.0
)
