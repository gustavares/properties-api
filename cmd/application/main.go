package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/portaria-app/properties-api/server/http"
	"github.com/portaria-app/properties-api/server/http/routes"
)

func main() {
	ctx := context.Background()

	r := http.NewRouter(routes.Setup())
	httpServer := http.NewServer(r.Router.Handler)
	httpServer.Run()

	shutdown := make(chan os.Signal, 2)
	signal.Notify(shutdown, syscall.SIGINT)

	<-shutdown
	if err := httpServer.Shutdown(ctx); err != nil {
		log.Fatal(err)
	}
}
